/* Le Duytam Ly
 * 1734119*/

package movies.importer;

import java.io.IOException;

public class ProcessingTest{

	public static void main(String[] args) throws IOException{
		
		//Using the LowercaseProcessor
		String source = "C:\\Users\\leduy\\courses\\java310\\TestInput";
		String destination = "C:\\Users\\leduy\\courses\\java310\\TestOutput";
		LowercaseProcessor lp = new LowercaseProcessor(source,destination);
		lp.execute();
		
		//Using the RemoveDuplcates (Uses the previous destination as source)
		String destinationDup = "C:\\Users\\leduy\\courses\\java310\\TestOutputDup";
		RemoveDuplicates rd = new RemoveDuplicates(destination,destinationDup);
		rd.execute();	
	}

}
