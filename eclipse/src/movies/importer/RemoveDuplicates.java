/* Le Duytam Ly
 * 1734119*/

package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor{
	
	/**
	 * Parameterized Constructor 
	 * @param sourceDir Source Directory
	 * @param outputDir Output Directory
	 */
	public RemoveDuplicates(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}
	
	@Override
	public ArrayList<String> process(ArrayList<String> input){
		ArrayList<String> noDuplicates = new ArrayList<String>();
		
		for (String s : input) {
			//Only adds into the new ArrayList if it did not appear in the new ArrayList
			if(!(noDuplicates.contains(s))) {
				noDuplicates.add(s);
			}
		}
		return noDuplicates;
	}

}
