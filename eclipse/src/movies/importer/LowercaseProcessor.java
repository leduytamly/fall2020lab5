/* Le Duytam Ly
 * 1734119*/

package movies.importer;

import java.util.ArrayList;

public class LowercaseProcessor extends Processor {
	
	/**
	 * Paramaterized Constructor
	 * @param sourceDir Source Directory
	 * @param outputDir Output Directory 
	 */
	public LowercaseProcessor(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}

	@Override
	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> asLower = new ArrayList<String>();
		
		for(String s : input) {
			asLower.add(s.toLowerCase());
		}
		
		return asLower;
	}

}
